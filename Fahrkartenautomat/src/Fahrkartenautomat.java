﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    
    {
    	for (int i = 1; i > 0; i++) {
    	
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    		double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
    		fahrkartenAusgeben();
    		rueckgeldAusgeben(rückgabebetrag);

    		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          	   "vor Fahrtantritt entwerten zu lassen!\n"+
                          	   "Wir wünschen Ihnen eine gute Fahrt.\n\n");
    	}
    }
    
    public static double fahrkartenbestellungErfassen() {
        Scanner tastatur = new Scanner(System.in);
        double[] fahrkarten = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
        String[] fahrkartenAusgabe = {"Einzelticket AB (1): 2,90", "Einzelticket BC (2): 3,30", "Einzelticket ABC (3): 3,60", "Kurzstrecke (4): 1,90", "Tagesticket Berlin AB (5): 8,60", "Tagesticket Berlin BC (6): 9,00", "Tagesticket Berlin ABC (7): 9,60", "Kleingruppen-Tagesticket Berlin AB (8): 23,50", "Kleingruppen-Tagesticket Berlin BC (9): 24,30", "Kleingruppen-Tagesticket Berlin ABC (10): 24,90"};
        int[] fahrkartenAnzahl = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int ticketAuswahl; 
        double zuZahlenderBetrag = 0;
        int ticketPrüfer = 0;
        double zwischenBetrag = 0;
        
        System.out.printf("Willkommen!\nEinzelticket AB (1): %s€\nEinzelticket BC (2): %s€\nEinzelticket ABC (3): %s€\nKurzstrecke (4): %s\nTagesticket Berlin AB (5): %s\nTagesticket Berlin BC (6): %s\nTagesticket Berlin ABC (7): %s\nKleingruppen-Tagesticket Berlin AB (8): %s\nKleingruppen-Tagesticket Berlin BC (9): %s\nKleingruppen-Tagesticket Berlin ABC (10): %s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
        
        while (ticketPrüfer == 0) {
        	ticketAuswahl = 0;
        	ticketAuswahl = tastatur.nextInt();
        	zwischenBetrag = 0;
        	int jaOderNein;
        	switch (ticketAuswahl) {
        	case 1:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	case 2:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	case 3:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	case 4:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	case 5:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	case 6:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	case 7:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	case 8:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	case 9:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	case 10:
        		System.out.print("Wieviele Tickets wollen Sie?: ");
        		fahrkartenAnzahl[ticketAuswahl - 1] = tastatur.nextInt();
        		zwischenBetrag = fahrkarten[ticketAuswahl - 1] * fahrkartenAnzahl[ticketAuswahl - 1];
        		System.out.print("Benötigen Sie mehr Tickets? JA (0) oder NEIN (1) : ");
        		jaOderNein = tastatur.nextInt();
        		if (jaOderNein == 1) {
        			ticketPrüfer = 1;
        			break;
        		} else if (jaOderNein == 0) {
                	System.out.printf("%s€\n%s€\n%s€\n%s\n%s\n%s\n%s\n%s\n%s\n%s\nGeben Sie bitte das gewünschte Ticket an: ", fahrkartenAusgabe[0], fahrkartenAusgabe[1], fahrkartenAusgabe[2], fahrkartenAusgabe[3], fahrkartenAusgabe[4], fahrkartenAusgabe[5], fahrkartenAusgabe[6], fahrkartenAusgabe[7], fahrkartenAusgabe[8], fahrkartenAusgabe[9]);
                	break;
        		}
        	}
            zuZahlenderBetrag = zuZahlenderBetrag + zwischenBetrag; 
        }
/*        while (ticketPrüfer == 0) {
        
        	ticketAuswahl = tastatur.nextInt();
        	if (ticketAuswahl == 1) {
        		ticketPreis = 2.30;
                int ticketAnzahlPrüfer = 0;
                
                while(ticketAnzahlPrüfer == 0) {
                	System.out.print("Anzahl der Tickets: ");
                	ticketAnzahl = tastatur.nextInt();
                	if (ticketAnzahl > 10 | ticketAnzahl < 1) {

                		System.out.print("Die Anzahl der Tickets war ungültig (Min=1 Max=10).\nErneute Eingabe: ");
                	}
                	else {
                		ticketAnzahlPrüfer = 1;
                	}
                }
                zwischenBetrag = ticketPreis * ticketAnzahl;
        	} else if (ticketAuswahl == 2) {
        		ticketPreis = 6.20;
                int ticketAnzahlPrüfer = 0;
                
                while(ticketAnzahlPrüfer == 0) {
                	System.out.print("Anzahl der Tickets: ");
                	ticketAnzahl = tastatur.nextInt();
                	if (ticketAnzahl > 10 | ticketAnzahl < 1) {

                		System.out.print("Die Anzahl der Tickets war ungültig (Min=1 Max=10).\nErneute Eingabe: ");
                	}
                	else {
                		ticketAnzahlPrüfer = 1;
                	}
                }
                zwischenBetrag = ticketPreis * ticketAnzahl;
        	} else if (ticketAuswahl == 3) {
        		ticketPreis = 14;
                int ticketAnzahlPrüfer = 0;
                
                while(ticketAnzahlPrüfer == 0) {
                	System.out.print("Anzahl der Tickets: ");
                	ticketAnzahl = tastatur.nextInt();
                	if (ticketAnzahl > 10 | ticketAnzahl < 1) {

                		System.out.print("Die Anzahl der Tickets war ungültig (Min=1 Max=10).\nErneute Eingabe: ");
                	}
                	else {
                		ticketAnzahlPrüfer = 1;
                	}
                }
                zwischenBetrag = ticketPreis * ticketAnzahl;
        	} else if (ticketAuswahl == 9) {
        		ticketPrüfer = 1;
        		zwischenBetrag = 0;
        	} else {
        		System.out.print("Ungültiges Ticket ausgewählt."); 	
        	}
        	if (ticketAuswahl != 9) {System.out.print("Möchten Sie weitere Tickets?\nEinzelticket (1): 2,30€\nTagesticket (2): 6,20€\nGruppenticket (3): 14,00€ oder wollen Sie bezahlen wählen Sie die 9.: ");
        } */

        

        
        

      
    	return zuZahlenderBetrag;
    	
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	double rückgabebetrag;
        eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        return rückgabebetrag;
    }
    
    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(1000);
        }
        System.out.println("\n\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
        if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
  	          //System.out.println(rückgabebetrag);
            }
        }
    }   
    
    public static void warte(int millisekunde) {
    	try {
        	Thread.sleep(millisekunde);
 		} catch (InterruptedException e) {
 			e.printStackTrace();
 		}

    }
}