
public class Temperaturtabelle {
	
	public static void main(String[] args) {
		
		String fahr = "Fahrenheit";
		String cel = "Celsius";
		String minus = "-";
		String strich = "|";
		
		// f�r die schleife
		int i = 1;
		int n = 23;
		
		int d1 = -20;
		int d2 = -10;
		int d3 = 0;
		int d4 = 20;
		int d5 = 30;
		
		System.out.printf("%-1s%3s%10s\n", fahr, strich, cel);

		while (i++ <= n) {
			System.out.printf("%s", minus);
		}
		
		double bruch = 0.55555555555;
		double dd1 = (d1-32)*bruch;
		double dd2 = (d2-32)*bruch;
		double dd3 = (d3-32)*bruch;
		double dd4 = (d4-32)*bruch;
		double dd5 = (d5-32)*bruch;
		
		System.out.printf("\n%-12d%s%10.2f", d1, strich, dd1);
		System.out.printf("\n%-12d%s%10.2f", d2, strich, dd2);
		System.out.printf("\n+%-11d%s%10.2f", d3, strich, dd3);
		System.out.printf("\n+%-11d%s%10.2f", d4, strich, dd4);
		System.out.printf("\n+%-11d%s%10.2f", d5, strich, dd5);
		
		
	}

}
