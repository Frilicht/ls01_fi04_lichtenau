import java.util.Scanner;
public class Mittelwert {

   public static void main(String[] args) {
	  
      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
      double x = liesDoubleWertEin("Bitte die erste Zahl eingeben: ");
      double y = liesDoubleWertEin("Bitte die zweite Zahl eingeben: ");
      double m;
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = mittelwert(x,y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   
   public static double liesDoubleWertEin(String frage) {
	   double zahl;
	   Scanner tastatur = new Scanner(System.in);
	   System.out.print(frage);	   
	   zahl = tastatur.nextDouble();
	   return zahl;
   }
   
   public static double mittelwert(double x, double y) {
	   double zahl;
	   zahl = (x + y)/2.0;
	   return zahl;
	   
   }
}
